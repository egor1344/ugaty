from __future__ import absolute_import, unicode_literals

from django.db import models
from django.views.decorators.cache import cache_page,cache_control
from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailimages.models import Image, AbstractImage, AbstractRendition
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel
from wagtail.wagtailembeds.blocks import EmbedBlock
from blog.models import BlogPage, AdPage, GalleryPage, InfoPage

class HomePage(Page):

    body = RichTextField(blank=True)
    number_menu = models.PositiveSmallIntegerField()

    # video = EmbedBlock()
    video = StreamField([
            ('video', EmbedBlock()),
        ], blank=True)
    url_raspisanie = models.ForeignKey(InfoPage, null=True, on_delete=models.SET_NULL, blank=True, related_name='+')
    page_in_top_nav = models.ForeignKey(Page, null=True, on_delete=models.SET_NULL, blank=True, related_name='+')
    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        # FieldPanel('video', classname="full"),
        PageChooserPanel('page_in_top_nav'),
        PageChooserPanel('url_raspisanie'),
        InlinePanel('gallery_images', label="Gallery images"),
        InlinePanel('partners_images', label="Partners images"),
        StreamFieldPanel('video'),
    ]

    @cache_control(max_age=3600)
    def get_context(self, request):
        context = super(HomePage, self).get_context(request)
        blogpages = BlogPage.objects.live().order_by('-date')[:8]
        context['blogpages'] = blogpages
        context['galerry'] = GalleryPage.objects.live().order_by('?')[:4]
        print(context['galerry'])
        adpages = AdPage.objects.live().order_by('date')[:4]
        context['adpages'] = adpages
        return context

class HomePageGalleryImage(Orderable):
    page = ParentalKey(HomePage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)
    url = models.URLField(blank=True)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
        FieldPanel('url'),
    ]

class Partners(Orderable):
    page = ParentalKey(HomePage, related_name='partners_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)
    url = models.URLField(blank=True)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
        FieldPanel('url'),
    ]
