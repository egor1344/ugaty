from django import template
from home.models import *
from blog.models import *

register = template.Library()

@register.assignment_tag(takes_context=True)
def get_site_root(context):
    return context['request'].site.root_page

def has_menu_children(page):
    return page.get_children().live().exists()

@register.inclusion_tag('home/top_menu/top_menu.html', takes_context=True)
def render_top_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().live().in_menu()
    sorted(menuitems, key=lambda menuitem: menuitem.specific.number_menu)
    m = [] # хосподи боже мой, простите меня за это
    for menuitem in menuitems:
        if menuitem.specific.place_menu == 'top':
            menuitem.show_dropdown = has_menu_children(menuitem)
            menuitem.active = (calling_page.url.startswith(menuitem.url)
                               if calling_page else False)
            m.append(menuitem)
    m = sorted(m, key=lambda m: m.specific.number_menu)
    return {
        'calling_page': calling_page,
        'menuitems': m,
        'request': context['request'],
    }

@register.inclusion_tag('home/top_menu/top_menu_children.html', takes_context=True)
def render_top_menu_child(context, parent, calling_page=None):
    menuitems = parent.get_children().live()
    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        'request': context['request'],
    }