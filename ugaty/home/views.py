from datetime import date

from django.shortcuts import render
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.loader import render_to_string
from wagtail.wagtailimages.models import Image

from .models import GalleryPage

def home(request):
    galery = GalleryPage.objects.live().order_by('date')


    month = {}
    year = []
    for g in galery:
        print(g.gallery.all())
        if g.date.year not in year:
            year.append(g.date.year)
        if g.date.month not in month.keys():
            month[g.date.month] = g.date

    print(year, month)

    page = request.GET.get('page')
    paginator = Paginator(galery, 12)  # Показывать 10 штук
    try:
        galery = paginator.page(page)
    except PageNotAnInteger:
        galery = paginator.page(1)
    except EmptyPage:
        galery = paginator.page(paginator.num_pages)

    return render(request,
                  'blog/all_gallery_page.html',
                  {'gallery': galery,
                   'year': year,
                   'month': month})

