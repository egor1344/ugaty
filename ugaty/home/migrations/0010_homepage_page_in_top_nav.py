# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-01-30 13:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('home', '0009_homepage_video'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='page_in_top_nav',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Page'),
        ),
    ]
