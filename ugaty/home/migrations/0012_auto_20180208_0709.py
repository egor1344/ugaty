# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-02-08 07:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0011_homepage_url_raspisanie'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='url_raspisanie',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='blog.InfoPage'),
        ),
    ]
