from django.shortcuts import render
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse

from .models import Table_annotate, Record, Annotate

# Create your views here.
def get_table(request):
    """ Отправка нужной таблицы по образовательным программамс """

    print('get_table', request.GET)
    type_table = request.GET.get('table', False)
    print(type_table)
    if not type_table:
        return False
    table = Table_annotate.objects.filter(name=type_table).first()
    if not table:
        return False
    print(table)
    record = table.record_set.order_by('order')
    print(record)
    html = render_to_string('annotate/table.html', {'record': record,
                                                    'type_table': type_table})
    return JsonResponse({'html': html})

def get_file(request):
    """ Отправка нужных файлов по образовательным программамс """

    type_table = request.GET.get('type_table', False)
    type_file  = request.GET.get('type_file', False)
    ids = request.GET.get('id', False)
    print(type_table, type_file, ids)
    if not ids:
        return False
    files = Annotate.objects.filter(types=type_file, record=ids)
    print(files)

    html = render_to_string('annotate/modal_file.html', {'files': files,
                                                         'type_table': type_table})
    return JsonResponse({'html': html})
