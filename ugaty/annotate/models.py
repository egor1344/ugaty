from datetime import time

from django.db import models
from django.utils.text import slugify

from wagtail.wagtaildocs.models import Document

class Table_annotate(models.Model):
    """ Таблицы для аннотаций """

    name = models.CharField('Название таблицы', max_length=100, unique=True, db_index=True)
    slug = models.SlugField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta():
        verbose_name = 'Таблица'
        verbose_name_plural = 'Таблицы'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)

        super(Table_annotate, self).save(*args, **kwargs)

class Record(models.Model):
    """ Запись в таблице """

    table = models.ForeignKey(Table_annotate)

    order = models.SmallIntegerField(default=0)
    cod = models.CharField('Код', max_length=50)
    name = models.CharField('Наименование специальности', max_length=500)
    level_obraz = models.CharField('Уровень образования', max_length=500)
    form_learn = models.CharField('Форма обучения', max_length=500)
    profile = models.CharField('Профиль', max_length=500)
    year = models.CharField('Год начала обучения', max_length=500)
    opisanie = models.ForeignKey(Document, related_name='opis')
    plan_learn = models.ForeignKey(Document, related_name='plan')
    electr_learn = models.CharField('Использовании электронного обучения', max_length=500)

    class Meta():
        verbose_name = 'Строка в таблице'
        verbose_name_plural = 'Строки в таблице'


class Annotate(models.Model):
    """ Аннотации """

    TYPE_FILE = (
        ("anotate", "Аннотации к рабочим программам дисциплин" ),
        ("plan_graf", "Календарный учебный график" ),
        ("metoda", "Методические и иные документы" ),
        )

    record = models.ForeignKey(Record, on_delete=models.CASCADE)
    file = models.ForeignKey(Document, related_name='annot')
    types = models.CharField(max_length=200, choices=TYPE_FILE)
