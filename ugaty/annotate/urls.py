from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^get_table/$', views.get_table, name='get_table'),
    url(r'^get_file/$', views.get_file, name='get_file'),
]
