from django.contrib import admin

from .models import Table_annotate, Record, Annotate

class Annotate_admin(admin.TabularInline):
    model = Annotate
    extra = 1
    raw_id_fields = ('file',)


class Record_admin(admin.ModelAdmin):
    inlines = [Annotate_admin,]
    list_display = ('cod', 'name', 'level_obraz', 'form_learn')
    list_filter = ('table',)
    raw_id_fields = ('opisanie', 'plan_learn',)

    class Media:
        css = {
            'all': ('record.css',)
        }

admin.site.register(Table_annotate)
admin.site.register(Record, Record_admin)
