from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.main, name='main'),
    url(r'^registration/$', views.registration, name='registration'),
]
