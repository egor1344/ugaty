# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-04-09 06:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('konference', '0008_auto_20180404_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sience_work',
            name='name',
            field=models.CharField(db_index=True, max_length=300, verbose_name='Название'),
        ),
    ]
