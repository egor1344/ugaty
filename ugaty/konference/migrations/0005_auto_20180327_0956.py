# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-03-27 09:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('konference', '0004_auto_20180321_0828'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sience_work',
            options={'verbose_name': 'Заяка', 'verbose_name_plural': 'Заявки'},
        ),
        migrations.AlterField(
            model_name='manager_member',
            name='sience',
            field=models.ManyToManyField(blank=True, related_name='manager_member', to='konference.Sience_work'),
        ),
        migrations.AlterField(
            model_name='member',
            name='sience',
            field=models.ManyToManyField(blank=True, related_name='member', to='konference.Sience_work'),
        ),
    ]
