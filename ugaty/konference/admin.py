from django.contrib import admin
from zipfile import ZipFile

from .exel import upload_to_exel
from .utils import upload_sert, get_sience_file

from .models import Sience_work, Section, Direction, Member, Manager_member

class Member_tab(admin.TabularInline):
    model = Member.sience.through
    extra = 1  

class Manager_member_tab(admin.TabularInline):
    model = Manager_member.sience.through
    extra = 1  

def upload_sience_work(modeladmin, request, queryset):
    return upload_to_exel(queryset)
upload_sience_work.short_description = "Вывод в Exel"

def get_sert(modeladmin, request, queryset):
    return upload_sert(queryset)
get_sert.short_description = "Вывод сертификатов"

def get_file(modeladmin, request, queryset):
    return get_sience_file(queryset)
get_file.short_description = "Скачать все файлы работ"

# def upload_file_sience_work(modeladmin, request, queryset):
#     files = ZipFile('barcode.zip', 'w')
# upload_file_sience_work.short_description = "Выгрузка файлов"


class Sience_work_admin(admin.ModelAdmin):
    inlines = [Member_tab, Manager_member_tab]
    list_display = ("status", 'name','treat','comments' , 'section', 'direction', 'get_member',
                    'get_member_email','get_member_tel' ,'get_place',
                    'get_classes', 'get_manager', 'get_manager_email','get_manager_tel')
    list_display_links = ('name',"status")
    list_filter = ('section', 'direction')
    actions = [upload_sience_work, get_sert, get_file]


    def get_member(self, obj):
        m = obj.member.all()
        s = ''
        for o in m:
            s += str(o) + '\n' 
        return s

    def get_place(self, obj):
        m = obj.member.all()
        s = ''
        for o in m:
            s += str(o.place) + '\n' 
        return s

    def get_classes(self, obj):
        m = obj.member.all()
        s = ''
        for o in m:
            s += str(o.clases) + '\n' 
        return s

    def get_member_email(self, obj):
        m = obj.member.all()
        s = ''
        for o in m:
            s += str(o.email) + '\n' 
        return s

    def get_member_tel(self, obj):
        m = obj.member.all()
        s = ''
        for o in m:
            s += str(o.tel) + '\n' 
        return s


    def get_manager(self, obj):
        m = obj.manager_member.all()
        s = ''
        for o in m:
            s += str(o) + '\n' 
        return s

    def get_manager_email(self, obj):
        m = obj.manager_member.all()
        s = ''
        for o in m:
            s += str(o.email) + '\n' 
        return s

    def get_manager_tel(self, obj):
        m = obj.manager_member.all()
        s = ''
        for o in m:
            s += str(o.tel) + '\n' 
        return s        


admin.site.register(Sience_work, Sience_work_admin)
admin.site.register(Section)
admin.site.register(Direction)
admin.site.register(Member)
admin.site.register(Manager_member)
