from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
from zipfile import ZipFile
import os
from django.http import HttpResponse

def render_sert(fio, teach_place, fio_ruk):
    #member.object.filter('first_name')
    # определяете шрифт
    font = ImageFont.truetype('konference/fonts/times.ttf',30)
    font2 = ImageFont.truetype('konference/fonts/timesbd.ttf')
    font3 = ImageFont.truetype('konference/fonts/timesi.ttf')
    # настройка
    # определяете положение текста на картинке
    fio_position = (330, 350)
    place_position = (330, 412)
    fio_ruk_position = (330, 475)
    # цвет текста, RGB
    fio_color = (0,0,0)
    place_color = (0,0,0)
    fio_ruk_color = (0,0,0)
    # собственно, сам текст
    fio = str(fio)
    place = str(teach_place)
    fio_ruk= str(fio_ruk)
    # загружаете фоновое изображение
    img = Image.open('default_sertifikat.jpg')

    # определяете объект для рисования
    draw = ImageDraw.Draw(img)

    # добавляем текст
    draw.text(fio_position, fio, fio_color, font )
    draw.text(place_position, place, place_color, font)
    draw.text(fio_ruk_position, fio_ruk, fio_ruk_color, font)
    # сохраняем новое изображение
    img.save('sertifikat_'+fio+'.jpg')
    return 'sertifikat_'+fio+'.jpg'

def upload_sert(qe):
    temp_file = []
    for q in qe:
        if q.member:
            for m in q.member.all():
                mem  = ''
                if q.manager_member:
                    for ms in q.manager_member.all():
                        mem += str(ms.fios()) + ""

                temp_file.append(render_sert(str(m), str(m.place), str(mem)))
    print(temp_file)
    with ZipFile('sert.zip', 'w') as mzip:
        for file in temp_file:
            mzip.write(file)
            os.remove(file)
        mzip.close()

    wrapper = open('sert.zip', 'rb')
    with  open('sert.zip', 'rb') as wrapper:
        content_type = 'application/zip'
        content_disposition = 'attachment; filename=sert.zip'

        response = HttpResponse(wrapper, content_type=content_type)
        response['Content-Disposition'] = content_disposition
        wrapper.close()
    os.remove('sert.zip')
    return response

def get_sience_file(qe):
    """ Скачивание всех файлов """

    with ZipFile('file.zip', 'w') as mzip:
        for file in qe:
            print(file.file_work.name)
            mzip.write('media/'+file.file_work.name)
        mzip.close()

    wrapper = open('file.zip', 'rb')
    os.remove('file.zip')
    content_type = 'application/zip'
    content_disposition = 'attachment; filename=file.zip'

    response = HttpResponse(wrapper, content_type=content_type)
    response['Content-Disposition'] = content_disposition

    return response


