from datetime import date

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string

from .models import Sience_work, Section, Direction, Member, Manager_member

def main(request):
    """ Регистрация на конференцию """

    sections = Section.objects.all()
    directions = Direction.objects.all()

    return render(request, "konference/main.html",
                  {'sections': sections,
                   'directions': directions})

@csrf_exempt
def registration(request):
    """ Регистрация """

    print(request.POST)

    member_one_fam = request.POST.get('member_familia', False)
    member_one_name = request.POST.get('member_name', False)
    member_one_otch = request.POST.get('member_otch', False)
    member_one_email = request.POST.get('member_email', False)
    # member_one_date = request.POST.get('member_date', False)
    member_one_tel = request.POST.get('member_tel', False)
    member_one_mesto = request.POST.get('member_mesto', False)
    member_one_class = request.POST.get('member_class', False)
    dates = request.POST.get('member_date', False)
    if dates:
        year, month, day = dates.split('-')

    if (member_one_fam and member_one_name and member_one_otch and member_one_tel and
        member_one_mesto and member_one_class and member_one_email):
        member, create = Member.objects.get_or_create(first_name=member_one_name,
                                                      mid_name=member_one_fam,
                                                      last_name=member_one_otch,
                                                      email=member_one_email,
                                                      tel=member_one_tel,
                                                      date=date(year=int(year), month=int(month), day=int(day)),
                                                      defaults={"place":member_one_mesto,
                                                                "clases":member_one_class})
    else:
        member = False

    member_two_fam = request.POST.get('member_two_familia', False)
    member_two_name = request.POST.get('member_two_name', False)
    member_two_otch = request.POST.get('member_two_otch', False)
    member_two_email = request.POST.get('member_two_email', False)
    dates = request.POST.get('member_two_date', False)
    if dates:
        year, month, day = dates.split('-')
    member_two_tel = request.POST.get('member_two_tel', False)
    member_two_mesto = request.POST.get('member_two_mesto', False)
    member_two_class = request.POST.get('member_two_class', False)

    if (member_two_fam and member_two_name and member_two_otch and member_two_tel and
        member_two_mesto and member_two_class and member_two_email):
        member_two, create = Member.objects.get_or_create(first_name=member_two_name,
                                                          mid_name=member_two_fam,
                                                          last_name=member_two_otch,
                                                          email=member_two_email,
                                                          date=date(year=int(year), month=int(month), day=int(day)),
                                                          tel=member_two_tel,
                                                          defaults={"place":member_two_mesto,
                                                                    "clases":member_two_class})
    else:
        member_two = False

    manage_one_fam = request.POST.get('manage_familia', False)
    manage_one_name = request.POST.get('manage_name', False)
    manage_one_otch = request.POST.get('manage_otch', False)
    manage_one_email = request.POST.get('manage_email', False)
    manage_one_tel = request.POST.get('manage_tel', False)
    manage_one_mesto = request.POST.get('manage_mesto', False)
    manage_one_dolz = request.POST.get('manage_dolznost', False)

    if (manage_one_fam and manage_one_name and manage_one_otch and manage_one_email and
        manage_one_tel and manage_one_mesto and manage_one_dolz):
        manage_one, create = Manager_member.objects.get_or_create(first_name=manage_one_name,
                                                                  mid_name=manage_one_fam,
                                                                  last_name=manage_one_otch,
                                                                  email=manage_one_email,
                                                                  tel=manage_one_tel,
                                                                  defaults={"place":manage_one_mesto,
                                                                            "clases":manage_one_dolz})
    else:
        manage_one = False

    manage_two_fam = request.POST.get('manage_two_familia', False)
    manage_two_name = request.POST.get('manage_two_name', False)
    manage_two_otch = request.POST.get('manage_two_otch', False)
    manage_two_email = request.POST.get('manage_two_email', False)
    manage_two_tel = request.POST.get('manage_two_tel', False)
    manage_two_mesto = request.POST.get('manage_two_mesto', False)
    manage_two_dolz = request.POST.get('manage_two_dolznost', False)
    if (manage_two_fam and manage_two_name and manage_two_otch and manage_two_email and
        manage_two_tel and manage_two_mesto and manage_two_dolz):
        manage_two, create = Manager_member.objects.get_or_create(first_name=manage_two_name,
                                                                  mid_name=manage_two_fam,
                                                                  last_name=manage_two_otch,
                                                                  email=manage_two_email,
                                                                  tel=manage_two_tel,
                                                                  defaults={"place":manage_two_mesto,
                                                                            "clases":manage_two_dolz})

    else:
        manage_two = False

    sections = request.POST.get('sections', None)
    directions = request.POST.get('directions', None)
    if sections != 'None':
        print(sections)
        sections = Section.objects.get(id=sections)
        directions = None
    elif directions != 'None':
        directions = Direction.objects.get(id=directions)
        sections = None
    else:
        sections = None
        directions = None

    name_work = request.POST.get('name_sience_work', False)
    treat = request.POST.get('treat', False)
    if treat == 'on':
        treat = True
    else:
        treat = False
    file_pay = request.FILES.get('pay', False)
    file_work = request.FILES.get('sience_work', False)

    if (member and manage_one and (sections or directions) and file_pay and file_work and name_work):
        sine_work, create = Sience_work.objects.get_or_create(name=name_work,
                                                              treat=treat,
                                                              section=sections,
                                                              direction=directions)
        sine_work.file_work = file_work
        sine_work.file_pay = file_pay
        if member_two:
            sine_work.member.set([member_two, member])
        else:
            sine_work.member.set([member])
        if manage_two:
            sine_work.manager_member.set([manage_two, manage_one])
        else:
            sine_work.manager_member.set([manage_one])
        sine_work.save()


    html = render_to_string('konference/message.html')


    return JsonResponse({'succes': True, 'html': html})