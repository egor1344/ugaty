from django.db import models

from datetime import time

class Section(models.Model):
    """ Список секций """

    name = models.CharField("Название", max_length=300, unique=True, db_index=True)


    def __str__(self):
        return self.name

    class Meta():
        verbose_name = 'Список секций'
        verbose_name_plural = 'Список секций'
        ordering = ['name']

class Direction(models.Model):
    """ Список направлений """

    name = models.CharField("Название", max_length=300, unique=True, db_index=True)

    def __str__(self):
        return self.name

    class Meta():
        verbose_name = 'Список направлений'
        verbose_name_plural = 'Список направлений'
        ordering = ['name']


class Sience_work(models.Model):
    """ Научная работа """

    STATUS_SIENCE_WORK = (
        ("Принято", 'Принято'),
        ("Не уточненные данные", 'Не уточненные данные'),
        ("Отказ", 'Отказ'),
        ("На рассмотрении", 'На рассмотрении'),
    )
    status = models.CharField(
        "Статус заявки",
        max_length=20,
        choices=STATUS_SIENCE_WORK,
        default="На рассмотрении",
    )


    name = models.CharField("Название", max_length=300, db_index=True)
    file_work = models.FileField(upload_to='file_work/')
    file_pay = models.FileField(upload_to="file_pay/")
    section = models.ForeignKey(Section, null=True, blank=True)
    direction = models.ForeignKey(Direction, null=True, blank=True)
    comments = models.TextField("Комментарии к заявке",null=True, blank=True)
    treat = models.BooleanField(default=False)
    def __str__(self):
        return self.name

    class Meta():
        verbose_name = 'Заяка'
        verbose_name_plural = 'Заявки'


class Member(models.Model):
    """ Участники """
    
    first_name = models.CharField("Имя", max_length=50, db_index=True)
    mid_name = models.CharField("Фамилия", max_length=50, db_index=True)
    last_name = models.CharField("Отчество", max_length=50, db_index=True)
    date = models.DateField("Дата рождения")
    tel = models.CharField("Телефон", max_length=50, db_index=True)
    place = models.TextField("Место учебы")
    clases = models.CharField("Класс, курс", max_length=15, db_index=True)
    sience = models.ManyToManyField(Sience_work, blank=True, related_name='member')
    email = models.CharField("Email", max_length=100, db_index=True)

    def __str__(self):
        return self.mid_name + ' ' + self.first_name + ' ' + self.last_name

    class Meta():
        verbose_name = 'Участники'
        verbose_name_plural = 'Участники'


class Manager_member(models.Model):
    """ Руководители """
    
    first_name = models.CharField("Имя", max_length=50, db_index=True)
    mid_name = models.CharField("Фамилия", max_length=50, db_index=True)
    last_name = models.CharField("Отчество", max_length=50, db_index=True)
    date = models.DateField("Дата рождения", null=True, blank=True)
    tel = models.CharField("Телефон", max_length=50, db_index=True)
    place = models.TextField("Место работы")
    clases = models.CharField("Должность", max_length=50, db_index=True)
    sience = models.ManyToManyField(Sience_work, blank=True, related_name='manager_member')
    email = models.CharField("Email", max_length=100, db_index=True)

    def __str__(self):
        return self.mid_name + ' ' + self.first_name + ' ' + self.last_name
    def fios(self):
        return self.mid_name + ' ' + self.first_name[0] + '.' + self.last_name[0]+ '.' 
    class Meta():
        verbose_name = 'Руководители'
        verbose_name_plural = 'Руководители'

