from django.apps import AppConfig


class KonferenceConfig(AppConfig):
    name = 'konference'
