from openpyxl import Workbook
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse

def upload_to_exel(qe):
    """ Перевод  в Exel """
    response = HttpResponse(content_type='application/vnd.ms-excel; charset=utf-8')
    response[
        'Content-Disposition'] = 'filename*=UTF-8""test_exel.xlsx'
    wb = Workbook()
    ws = wb.active
    ws.append(["Заявки"])
    print(qe)
    ws.append([
            'Номер заявки',
            'Название',
            'Добавление в сборник',
            'Секция',
            'Коментарии',
            'Направление',
            'Участники',
            "Email участников",
            "Телефон участников",
            'Место учебы',
            'Класс\курс',
            'Руководители',
            'Email руководителей',   
            'Телефоны руководителей'])
    for q in qe:
        m = q.member.all()
        member = ''
        for o in m:
            member += str(o) + '\n'
        email_member = ''
        for o in m:
            email_member += str(o.email) + '\n'
        tel_member = ''
        for o in m:
            tel_member += str(o.tel) + '\n'
        place = ''
        for o in m:
            place += str(o.place) + '\n'
        classes = ''
        for o in m:
            classes += str(o.clases) + '\n'
        m = q.manager_member.all()
        manager = ''
        for o in m:
            manager += str(o) + '\n'
        email_manager = ''
        for o in m:
            email_manager += str(o.email) + '\n'
        tel_manager = ''
        for o in m:
            tel_manager += str(o.tel) + '\n'
        ws.append([
                str(q.id),
                str(q.name),
                str(q.treat),
                str(q.section),
                str(q.comments),
                str(q.direction),
                member,
                email_member,
                tel_member,
                place,
                classes,
                manager,
                email_manager,   
                tel_manager])        
    for i in range(qe.count()):
        print(ws.row_dimensions[i+3].height)
        ws.row_dimensions[i+3].height = 30
        print(ws.row_dimensions[i+3].height)
    wb.save(response)
    return response