from django.shortcuts import render

from .models import Message

# Create your views here.
def main(request):
    """ Приемная коммисия """

    return render(request, 'priemnaya/main.html')

def get_message(request):
    """ Получение обращения """

    print(request.POST)

    name = request.POST.get('name', False)
    email = request.POST.get('email', False)
    text = request.POST.get('text', False)

    if name and email and text:
        Message.objects.create(name=name, email_tel=email, message=text)
        return render(request, 'priemnaya/main.html',
                      {'status': 'Done'})
    else:
        return render(request, 'priemnaya/main.html',
                      {'status': 'Error'})
