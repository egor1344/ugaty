from django.db import models

class Message(models.Model):
    """ Список секций """

    name = models.CharField("От кого", max_length=300)
    email_tel = models.CharField("Способ связаться", max_length=300)
    message = models.TextField("Обращение")


    def __str__(self):
        return "{} {}".format(self.name, self.email_tel)

    class Meta():
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['name']