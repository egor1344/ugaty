$("form#main_form").submit(function(e) {
    /* Act on the event */
    e.preventDefault();
    var formElement = document.querySelector("form");
    var formDatas = new FormData(formElement);
    $.ajax({
        url: $("#main_form").attr('action'),
        type: 'POST',
        // dataType: "json",
        data: formDatas,
        processData: false,
        contentType: false,
        success: function (value) {
            console.log(value);
            if (value.succes) {
                $('#success').modal('show');
                $(".suc").html(value.html);
            }
        },
    });

});

$("#i_check").change(function(event) {
    /* Act on the event */
    if ($("#i_check").prop('checked')) {
        $('#get').removeClass('disabled');
        $('#get').removeAttr('aria-disabled');
    } else {
        $('#get').addClass('disabled');
        $('#get').attr('aria-disabled', 'true');
    }
    $("select").change();    
});

$("select").change(function(event) {
    /* Act on the event */
    if ((($("#select01").val() == 'None') && ($("#select0").val() != 'None')) || (($("#select01").val() != 'None') && ($("#select0").val() == 'None'))) {
        $('#get').removeClass('disabled');
        $('#get').removeAttr('aria-disabled');
    } else {
        $('#get').addClass('disabled');
        $('#get').attr('aria-disabled', 'true');
    }
    $("#i_check").change();
});