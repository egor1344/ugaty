glaz = true

$('#glaz').on('click', function(event) {
    if (glaz) {
        glaz = false

        $('.glaz_body').append('<button type="button" class="btn btn-light font_size font_h4"><h4>+A</h4></button>');
        $('.glaz_body').append('<button type="button" class="btn btn-light font_size font_h3"><h3>+A</h3></button>');
        $('.glaz_body').append('<button type="button" class="btn btn-light font_size font_h2"><h2>+A</h2></button>');
        $('.glaz_body').append('<button type="button" class="btn btn-light glaz_back">Обычная версия сайта</button>');
        $('.glaz_body').addClass('glaz_body_active');
        $('.navbar').removeClass('nav-blue').addClass('nav-blue_glaz');
        $('.navbar-brand').addClass('glaz');
        $('.top_bottom_menu').addClass('glaz');
        $('.top_bottom_menu a').addClass('glaz');
        $('.img-zagl').addClass('glaz');
        $('.prewie').addClass('glaz');
        $('.two_navs_container').addClass('glaz');
        $('.logo img').addClass('glaz_img');
        $('.slider').removeClass('col-lg-9').addClass('noslider');
        $('.raspisane').removeClass('col-lg-3').addClass('col-lg-12').addClass('glaz');
        $('.rasp_header').addClass('glaz');
        $('.rasp_header b').addClass('number_week_glaz');
        $('.rasp').addClass('glaz');
        $('.rasp a').addClass('glaz');
        $('.anons h3').addClass('glaz');
        $('.anons_body').addClass('anons_body_glaz');
        $('.anons_body_glaz').removeClass('anons_body');
        $('.head_news div h4').addClass('glaz');
        $('.head_news div a').removeClass('all_news').addClass('all_news_glaz');
        $('.news img').addClass('glaz_img');
        $('.news div h5 a').addClass('glaz');
        $('.news div div').addClass('glaz');
        $('.news div p').addClass('glaz');
        $('.wrapper_fluid').addClass('wrapper_fluid_glaz');
        $('.head_video').addClass('glaz');
        $('.partners').addClass('glaz');
        $('.container').addClass('glaz');
        $('footer').addClass('footer_glaz');
        $('.dropdown-menu').addClass('dropdown-menu_glaz');
        $('.dropdown-item').addClass('dropdown-item_glaz');
        $('.li_menu_right').addClass('li_menu_right_glaz');
        $('#glaz').addClass('glaz_hide');            
    } else {
        $('.glaz_body').removeClass('glaz_body_active');
        $('.navbar').removeClass('nav-blue_glaz').addClass('nav-blue');                
        $('.navbar-brand').removeClass('glaz');
        $('.top_bottom_menu').removeClass('glaz');
        $('.top_bottom_menu a').removeClass('glaz');
        $('.img-zagl').removeClass('glaz');
        $('.prewie').removeClass('glaz');
        $('.two_navs_container').removeClass('glaz');
        $('.logo img').removeClass('glaz_img');
        $('.slider').removeClass('noslider').addClass('col-lg-9');
        $('.raspisane').removeClass('glaz').removeClass('col-lg-12').addClass('col-lg-3');
        $('.rasp_header').removeClass('glaz');
        $('.rasp_header b').removeClass('number_week_glaz');
        $('.rasp').removeClass('glaz');
        $('.rasp a').removeClass('glaz');
        $('.anons h3').removeClass('glaz');
        $('.anons_body_glaz').addClass('anons_body');
        $('.anons_body').removeClass('anons_body_glaz');
        $('.head_news div h4').removeClass('glaz');
        $('.head_news div a').removeClass('all_news').removeClass('all_news_glaz');
        $('.news img').removeClass('glaz_img');
        $('.news div h5 a').removeClass('glaz');
        $('.news div div').removeClass('glaz');
        $('.news div p').removeClass('glaz');
        $('.wrapper_fluid').removeClass('wrapper_fluid_glaz');
        $('.head_video').removeClass('glaz');
        $('.partners').removeClass('glaz');
        $('.container').removeClass('glaz');
        $('footer').removeClass('footer_glaz');
        $('.dropdown-menu').removeClass('dropdown-menu_glaz');
        $('.dropdown-item').removeClass('dropdown-item_glaz');
        $('.li_menu_right').removeClass('li_menu_right_glaz');
        $('#glaz').removeClass('glaz_hide');            
        glaz = true
        $('#glaz_back').remove();        
        $('.font_h4').remove();        
        $('.font_h3').remove();        
        $('.font_h2').remove();        
    }
    console.log(glaz, $('.logo').length)
});

$('body').on('click', '.glaz_back', function(event) {
    if (glaz) {
        $('.navbar').removeClass('nav-blue').addClass('nav-blue_glaz');
        $('.glaz_body').addClass('glaz_body_active');
        $('.navbar-brand').addClass('glaz');
        $('.top_bottom_menu').addClass('glaz');
        $('.top_bottom_menu a').addClass('glaz');
        $('.img-zagl').addClass('glaz');
        $('.prewie').addClass('glaz');
        $('.two_navs_container').addClass('glaz');
        $('.logo img').addClass('glaz_img');
        $('.slider').removeClass('col-lg-9').addClass('noslider');
        $('.raspisane').removeClass('col-lg-3').addClass('col-lg-12').addClass('glaz');
        $('.rasp_header').addClass('glaz');
        $('.rasp_header b').addClass('number_week_glaz');
        $('.rasp').addClass('glaz');
        $('.rasp a').addClass('glaz');
        $('.anons h3').addClass('glaz');
        $('.anons_body').addClass('anons_body_glaz');
        $('.head_news div h4').addClass('glaz');
        $('.head_news div a').removeClass('all_news').addClass('all_news_glaz');
        $('.news img').addClass('glaz_img');
        $('.news div h5 a').addClass('glaz');
        $('.news div div').addClass('glaz');
        $('.news div p').addClass('glaz');
        $('.wrapper_fluid').addClass('wrapper_fluid_glaz');
        $('.head_video').addClass('glaz');
        $('.partners').addClass('glaz');
        $('.container').addClass('glaz');
        $('footer').addClass('footer_glaz');
        $('.dropdown-menu').addClass('dropdown-menu_glaz');
        $('.dropdown-item').addClass('dropdown-item_glaz');
        $('#glaz').addClass('glaz_hide');            
        glaz = false        
    } else {
        $('.navbar').removeClass('nav-blue_glaz').addClass('nav-blue').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');                
        $('.navbar-brand').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.glaz_body').removeClass('glaz_body_active');
        $('.top_bottom_menu').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.top_bottom_menu a').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.img-zagl').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.prewie').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.two_navs_container').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.logo img').removeClass('glaz_img').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.slider').removeClass('noslider').addClass('col-lg-9').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.raspisane').removeClass('glaz').removeClass('col-lg-12').addClass('col-lg-3').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.rasp_header').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.rasp_header b').removeClass('number_week_glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.rasp').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.rasp a').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.anons h3').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.anons_body_glaz').addClass('anons_body');
        $('.anons_body').removeClass('anons_body_glaz');
        $('.anons_body').removeClass('anons_body_glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.head_news div h4').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.head_news div a').removeClass('all_news').removeClass('all_news_glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.news img').removeClass('glaz_img').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.news div h5 a').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.news div div').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.news div p').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.wrapper_fluid').removeClass('wrapper_fluid_glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.head_video').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.partners').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.container').removeClass('glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('footer').removeClass('footer_glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.dropdown-menu').removeClass('dropdown-menu_glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.dropdown-item').removeClass('dropdown-item_glaz').removeClass('f_h2').removeClass('f_h3').removeClass('f_h4');
        $('.li_menu_right').removeClass('li_menu_right_glaz');
        $('#glaz').removeClass('glaz_hide');            
        glaz = true;
        $('.font_h4').remove();        
        $('.font_h3').remove();        
        $('.font_h2').remove();        
        $('#glaz_back').remove();        
    }
    console.log(glaz)
    $parent = $(this).parent();
    $(this).remove();
});

$('body').on('click', '.font_size', function(event) {

    console.log($(this), $(this).hasClass('font_h2'));

    if ($(this).hasClass('font_h2')) {
        console.log('h2 click');
        var c = 'f_h2';
        var cDel1 = 'f_h3';
        var cDel2 = 'f_h4';
    } else if ($(this).hasClass('font_h3')) {
        console.log('h3 click');
        var c = 'f_h3';
        var cDel1 = 'f_h2';
        var cDel2 = 'f_h4';
    } else if ($(this).hasClass('font_h4')) {
        console.log('h4 click');
        var c = 'f_h4';
        var cDel1 = 'f_h3';
        var cDel2 = 'f_h2';
    }
    $('.navbar').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.navbar-brand').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.top_bottom_menu').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.top_bottom_menu a').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.img-zagl').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.prewie').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.two_navs_container').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.logo img').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.slider').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.raspisane').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.rasp_header').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.rasp_header b').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.rasp').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.rasp a').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.anons h3').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.anons_body').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.head_news div h4').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.head_news div a').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.news img').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.news div h5 a').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.news div div').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.news div p').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.wrapper_fluid').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.head_video').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.partners').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.container').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('footer').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.dropdown-menu').removeClass(cDel1).removeClass(cDel2).addClass(c);
    $('.dropdown-item').removeClass(cDel1).removeClass(cDel2).addClass(c);
});