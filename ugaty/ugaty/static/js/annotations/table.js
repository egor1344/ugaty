$('#accordion').on('click', '#open_table', function(event) {
  var in_table = $(this).attr('js-table');
  console.log('asdf');
  $.ajax({
    url: '/annotate/get_table',
    type: 'GET',
    dataType: 'json',
    data: {table: in_table},
  })
  .done(function (argument) {
    // body...
    console.log($(in_table))
    $('#'+in_table).html(argument.html);
  })
});

$('#accordion').on('click', '#file', function(event) {
  var type_table = $(this).attr('js-type-table');
  var type_file = $(this).attr('js-type-file');
  var id = $(this).attr('js-id-rec');
  $.ajax({
    url: '/annotate/get_file',
    type: 'GET',
    dataType: 'json',
    data: {type_table: type_table, type_file: type_file, id: id},
  })
  .done(function (argument) {
    // body...
    $('#'+type_table+'_content').html(argument.html);
  })
});