$('select').change(function(event) {
    /* Act on the event */
    $cat = $('#category');
    $year = $('#year');
    $month = $('#month');

    console.log($cat, $cat.val());

    $.ajax({
        url: 'ajax/change_month',
        type: 'GET',
        dataType: 'json',
        data: {cat: $cat.val(), year: $year.val(),
               month: $month.val()},
        success: function(data) {
            console.log(data);
            $('#main').html(data.html);
            $('#pag').html(data.pag);
        },
    });
});