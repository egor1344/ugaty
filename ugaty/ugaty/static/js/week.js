function countWeek() {
// массив дат "начал отсчета" в формате [месяц, день], в возрастющем порядке
   var stops = [ [8,2], [1,9] ];
   var now = new Date(), year = now.getFullYear(), start, tmp;
   start = tmp = new Date(year-1, stops[stops.length-1][1]-1, stops[stops.length-1][0]);
   for (var i=0; i < stops.length; i++) { 
      tmp = new Date(year, stops[i][1]-1, stops[i][0]);
      if (tmp <= now) start = tmp;
   }
   start -= start.getDay()*86400000;
   return Math.ceil((now-start)/86400000/7)-1;
   //вариант со смещением
   //   return Math.ceil((now-start)/86400000/7)+1;
}
//Текущая дата
var nowDate = new Date();
var curMonth = nowDate.getMonth()+1;
if (curMonth<9&&curMonth>6) {
   $('.rasp_header').remove(); 
}
else
{
   $('.number_week').text(countWeek())
}

