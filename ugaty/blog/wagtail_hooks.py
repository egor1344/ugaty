from wagtail.wagtailcore import hooks
from django.utils.text import slugify
from unidecode import unidecode

@hooks.register('before_edit_page')
def before_edit_page(request, parent_page, page_class=None):
    parent_page.slug = slugify(unidecode(parent_page.slug))
    parent_page.save()
