from datetime import date

from django.shortcuts import render
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.loader import render_to_string
from wagtail.wagtailimages.models import Image

from .models import GalleryPage

def all_page_galery(request):
    galery = GalleryPage.objects.live().order_by('date')


    month = {}
    year = []
    for g in galery:
        print(g.gallery.all())
        if g.date.year not in year:
            year.append(g.date.year)
        if g.date.month not in month.keys():
            month[g.date.month] = g.date

    print(year, month)

    page = request.GET.get('page')
    paginator = Paginator(galery, 12)  # Показывать 10 штук
    try:
        galery = paginator.page(page)
    except PageNotAnInteger:
        galery = paginator.page(1)
    except EmptyPage:
        galery = paginator.page(paginator.num_pages)

    return render(request,
                  'blog/all_gallery_page.html',
                  {'gallery': galery,
                   'year': year,
                   'month': month})

def change_month(request):
    """ Изменение месяца """

    cat = request.GET.get('cat', None)
    year = request.GET.get('year', None)
    month = request.GET.get('month', None)
    
    print(cat, year, month)

    if cat == '1':
        galery = GalleryPage.objects.live().order_by('date')
    else:
        galery = GalleryPage.objects.filter(gallery__image__tagged_items__tag__name=cat, gallery__image__title=cat)

    if year != "Все":
        if month != "Все":
            dates = date(int(year), int(month), 1)
            galery = galery.filter(date__year=dates.year, date__month=dates.month)
        else:
            dates = date(int(year), 1, 1)
            galery = galery.filter(date__year=dates.year)


    # print(galery)       

    # print(cat, year, month)


    page = request.GET.get('page')
    paginator = Paginator(galery, 12)  # Показывать 10 штук
    try:
        galery = paginator.page(page)
    except PageNotAnInteger:
        galery = paginator.page(1)
    except EmptyPage:
        galery = paginator.page(paginator.num_pages)
    
    rendered = render_to_string('blog/include/galerry_all.html', {'gallery': galery})
    pag = render_to_string('blog/include/gallery_paginations.html', {'gallery': galery})

    return JsonResponse({'html': rendered, 'pag': pag})
