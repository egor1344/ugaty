from django.db import models

from modelcluster.fields import ParentalKey
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailcore import blocks
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailcore.blocks import RawHTMLBlock
from wagtail.wagtaildocs.blocks import DocumentChooserBlock
from wagtail.contrib.table_block.blocks import TableBlock

class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron

        blogpages_all = BlogPage.objects.live().order_by('-date')

        page = request.GET.get('page')
        paginator = Paginator(blogpages_all, 10)  # Показывать 10 постов
        try:
            blogpages = paginator.page(page)
        except PageNotAnInteger:
            blogpages = paginator.page(1)
        except EmptyPage:
            blogpages = paginator.page(paginator.num_pages)

        context = super(BlogIndexPage, self).get_context(request)
        # print(context)
        # print(blogpages)
        context['blogpages'] = blogpages
        context['blogpages_all'] = blogpages_all
        return context

class BlogPage(Page):
    menu = (
        ('top', 'Top menu'),
        ('bottom', 'Bottom menu'),
    )
    place_menu = models.CharField(
        max_length=7,
        choices=menu,
        default='bottom',
    )

    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250, blank=True)
    number_menu = models.PositiveSmallIntegerField(blank=True, null=True, default=0)
    body = StreamField([
        ('heading', blocks.CharBlock(classname="full title", default='')),
        ('paragraph', blocks.RichTextBlock(default='')),
        ('table', TableBlock(template="blog/table/tables.html")),
        ('image', ImageChooserBlock()),
        ('html', RawHTMLBlock()),
        ('documents', DocumentChooserBlock())
    ], blank=True)

    gallery_page = models.ForeignKey('blog.GalleryPage', null=True, on_delete=models.SET_NULL, blank=True, related_name='+')

    search_fields = Page.search_fields + [
        index.SearchField('intro', partial_match=True),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('place_menu'),
        ImageChooserPanel('main_image'),
        PageChooserPanel('gallery_page'),
        FieldPanel('number_menu'),
        FieldPanel('intro'),
        # InlinePanel('gallery_images', label="Gallery images"),
        StreamFieldPanel('body'),
    ]

class InfoPage(Page):
    menu = (
        ('top', 'Top menu'),
        ('bottom', 'Bottom menu'),
    )
    place_menu = models.CharField(
        max_length=7,
        choices=menu,
        default='bottom',
    )

    intro = models.CharField(max_length=250, blank=True)
    body = StreamField([
        ('heading', blocks.CharBlock(classname="full title", default='')),
        ('paragraph', blocks.RichTextBlock(default='')),
        ('table', TableBlock(template="blog/table/tables.html")),
        ('image', ImageChooserBlock()),
        ('html', RawHTMLBlock()),
        ('documents', DocumentChooserBlock())
    ], blank=True)
    number_menu = models.PositiveSmallIntegerField(blank=True, null=True)
    urls = models.URLField(blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('intro', partial_match=True),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        FieldPanel('urls'),    
        FieldPanel('place_menu'),
        FieldPanel('number_menu'),
        StreamFieldPanel('body'),
    ]

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super(InfoPage, self).get_context(request)
        is_menu = self.get_children().live()
        if is_menu:
            context['is_menu'] = True
        else:
            context['is_menu'] = False             
        # print(context['is_menu'])
        # print(request)
        return context


class AdPage(Page):
    intro = models.CharField(max_length=100, blank=True)
    date = models.DateField("Post date")
    body = StreamField([
        ('heading', blocks.CharBlock(classname="full title", default='')),
        ('paragraph', blocks.RichTextBlock(default='')),
        ('table', TableBlock(template="blog/table/tables.html")),
        ('image', ImageChooserBlock()),
        ('html', RawHTMLBlock()),
        ('documents', DocumentChooserBlock())
    ], blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        FieldPanel('date'),
        StreamFieldPanel('body'),
    ]


class GalleryPage(Page):
    intro = models.CharField(max_length=100, blank=True)

    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='image')
    date = models.DateField("Post date")
    body = StreamField([
        ('heading', blocks.CharBlock(classname="full title", default='')),
        ('paragraph', blocks.RichTextBlock(default='')),
        ('table', TableBlock(template="blog/table/tables.html")),
        ('image', ImageChooserBlock()),
        ('html', RawHTMLBlock()),
        ('documents', DocumentChooserBlock())
    ], blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('intro', partial_match=True),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        FieldPanel('date'),
        ImageChooserPanel('main_image'),
        StreamFieldPanel('body'),
        InlinePanel('gallery', label="Gallery images"),
    ]

class BlogPageGalleryImage(Orderable):
    page = ParentalKey(GalleryPage, related_name='gallery')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='image_r')
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]
