from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^gallery/$', views.all_page_galery, name='all_page_galery'),
    url(r'^gallery/ajax/change_month/$', views.change_month, name='change_month'),
]
