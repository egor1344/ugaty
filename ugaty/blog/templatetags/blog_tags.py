from django import template
from home.models import *
from blog.models import *

register = template.Library()

@register.assignment_tag(takes_context=True)
def get_site_root(context):
    print(context['request'])
    return context['request'].site.root_page

def has_menu_children(page):
    return page.get_children().live().exists()

@register.inclusion_tag('blog/tags/top_menu.html', takes_context=True)
def top_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().live().in_menu()
    m = []
    for menuitem in menuitems:
        if menuitem.specific.place_menu == 'bottom':
            menuitem.show_dropdown = has_menu_children(menuitem)
            menuitem.active = (calling_page.url.startswith(menuitem.url)
                               if calling_page else False)
            m.append(menuitem)
    m = sorted(m, key=lambda m: m.specific.number_menu)
    return {
        'calling_page': calling_page,
        'menuitems': m,
        'request': context['request'],
    }

@register.inclusion_tag('blog/tags/right_menu.html', takes_context=True)
def right_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().live().in_menu()
    print(parent.get_parent(), parent.get_parent().url)
    # print(menuitems)
    # Меню будет подерживать 2 уровня вложенности, что нормальное делать нет
    # желания и сил
    is_home = ((parent.get_parent().url == '/')
                               if True else False)
    is_home_2 = ((parent.get_parent().get_parent().url == '/')
                               if True else False)
    if menuitems:
        for menuitem in menuitems:
            menuitem.show_dropdown = has_menu_children(menuitem)
            menuitem.active = (calling_page.url.startswith(menuitem.url)
                               if calling_page else False)
    elif is_home:
        menuitems = []
    else:
        if not is_home_2:
            menuitems = parent.get_parent().get_parent().get_children().live()
            for menuitem in menuitems:
                menuitem.show_dropdown = has_menu_children(menuitem)
                menuitem.active = (calling_page.url.startswith(menuitem.url)
                                   if calling_page else False)
        else:
            menuitems = parent.get_parent().get_children().live().in_menu()
            for menuitem in menuitems:
                menuitem.show_dropdown = has_menu_children(menuitem)
                menuitem.active = (calling_page.url.startswith(menuitem.url)
                                   if calling_page else False)
    menuitems = sorted(menuitems, key=lambda menuitems: menuitems.specific.number_menu)                

    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        'request': context['request'],
    }

@register.inclusion_tag('blog/tags/right_menu_children.html', takes_context=True)
def right_menu_children(context, parent, calling_page=None):
    menuitems = parent.get_children().live()
    # print(menuitems)
    menuitems = sorted(menuitems, key=lambda menuitems: menuitems.specific.number_menu)                
    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        'request': context['request'],
    }
