from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index

from blog.models import BlogPage, AdPage

class LandingPage(Page):

    main_text = RichTextField(blank=True)
    small_text = RichTextField(blank=True)
    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        FieldPanel('main_text'),
        FieldPanel('small_text'),
        ImageChooserPanel('main_image'),
    ]

    template = 'landing/main_landing.html'

    def get_context(self, request):
        context = super(LandingPage, self).get_context(request)
        # print(BlogPage.objects.all())
        blogpages = BlogPage.objects.live().order_by('-first_published_at')[:6]
        context['blogpages'] = blogpages
        adpages = AdPage.objects.live().order_by('-first_published_at')[:3]
        context['adpages'] = adpages
        return context
